import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxEditorModule } from 'ngx-editor';
import { HttpClientModule } from '@angular/common/http';







import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/navigation/header/header.component';
import { SidenavComponent } from './components/navigation/sidenav/sidenav.component';
import { CovalentModule } from './coalent.module';
import { HomeComponent } from './components/home/home.component';
import { HotComponent } from './components/home/hot/hot.component';
import { YourdiscussionsComponent } from './components/home/yourdiscussions/yourdiscussions.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HotDiscussionComponent } from './components/home/hot/hot-discussion/hot-discussion.component';
import { CreateDiscussionComponent } from './components/create-discussion/create-discussion.component';
import { LoginComponent } from './components/auth/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    HomeComponent,
    HotComponent,
    YourdiscussionsComponent,
    HotDiscussionComponent,
    CreateDiscussionComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    CovalentModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxEditorModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
