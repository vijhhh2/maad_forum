import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hot',
  templateUrl: './hot.component.html',
  styleUrls: ['./hot.component.css']
})
export class HotComponent implements OnInit {
  date;
  month;
  year;
  constructor() { }

  ngOnInit() {
  this.date = new Date().getDate().toString();
  this.month = new Date().getMonth().toString();
  this.year = new Date().getFullYear().toString();
  }

}
