import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('sideNav') private sideNav: MatSidenav;
  public innerWidth: any;
  snToggle: boolean;
  constructor() {}
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  ngOnInit() {
    this.innerWidth = window.innerWidth;
    console.log(this.snToggle);
  }
  onToggle() {
  this.sideNav.toggle();
  }
}
