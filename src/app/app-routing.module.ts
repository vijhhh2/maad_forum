import { CreateDiscussionComponent } from './components/create-discussion/create-discussion.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotDiscussionComponent } from './components/home/hot/hot-discussion/hot-discussion.component';
import { HomeComponent } from './components/home/home.component';

export const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'hotdiscussion', component: HotDiscussionComponent},
  {path: 'creatediscussion', component: CreateDiscussionComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes)],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
