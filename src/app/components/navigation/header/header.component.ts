import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  HostListener,
  Inject
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // fixed: boolean;
  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(@Inject(DOCUMENT) private doc: Document) {}

  ngOnInit(): void {}
  onToggleSidenav() {
  this.toggleSidenav.emit();
  }
  // @HostListener('window:scroll', [])
  // onWindowScroll() {
  //   const num2 = this.doc.documentElement.scrollTop;
  //   if (num2 > 50) {
  //     this.fixed = true;
  //   } else if (this.fixed && num2 < 5) {
  //     this.fixed = false;
  //   }
  // }
}
