import { Component, OnInit, ViewChild } from '@angular/core';
import { StepState } from '@covalent/core/steps';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';

export interface CreateDiscussion {
  room: string;
  discussionTitle: string;
  discussionDescription: string;
}

@Component({
  selector: 'app-create-discussion',
  templateUrl: './create-discussion.component.html',
  styleUrls: ['./create-discussion.component.css']
})
export class CreateDiscussionComponent implements OnInit {
  createGroup: FormGroup;

  rooms =  ['Idea', 'Technical'];
constructor(private _fb: FormBuilder) {}

  ngOnInit() {
  this.createGroup = this._fb.group({
    'room': this._fb.control('Idea', Validators.required),
    'discussionTitle': this._fb.control('', Validators.required),
    'description': this._fb.control('', Validators.required),
  });
  }
onSubmit() {
console.log(this.createGroup.value);
}

}
