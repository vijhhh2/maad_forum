import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hot-discussion',
  templateUrl: './hot-discussion.component.html',
  styleUrls: ['./hot-discussion.component.css']
})
export class HotDiscussionComponent implements OnInit {

  answers = [];
  answerMode = false;
  editorValue = '';
  config = {
    'editable': true,
    'spellcheck': true,
    'height': 'auto',
    'minHeight': '500px',
    'width': '1000px',
    'minWidth': '0',
    'translate': 'yes',
    'enableToolbar': true,
    'showToolbar': true,
    'placeholder': 'Please Wirite your answer',
    'imageEndPoint': '',
    'toolbar': [
        ['bold', 'italic', 'underline', 'superscript', 'subscript'],
        ['fontName', 'fontSize', 'color'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'indent', 'outdent'],
        ['cut', 'copy', 'delete', 'removeFormat', 'undo', 'redo'],
        ['paragraph', 'blockquote', 'removeBlockquote', 'horizontalLine', 'orderedList', 'unorderedList'],
        ['link', 'unlink']
    ]
};
  constructor() { }

  ngOnInit() {
  }
  onAnswer() {
    this.answerMode = !this.answerMode;
  }
  onReport() {}
  onSubmit() {
   this.answers.push(this.editorValue);
  }

}
