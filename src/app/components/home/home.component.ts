import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { MatTab } from '@angular/material';
import { BasePortalOutlet } from '@angular/cdk/portal';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {


  constructor() { }

  ngOnInit() {

  }
  ngAfterViewInit(): void {

  }


}
