import { NgModule } from '@angular/core';
import {  CovalentNotificationsModule,
  CovalentSearchModule,
  CovalentMenuModule,
  CovalentCommonModule,
  CovalentLayoutModule,
  CovalentMediaModule,
  CovalentExpansionPanelModule,
  CovalentStepsModule,
  CovalentDialogsModule,
  CovalentLoadingModule,
  CovalentPagingModule,
  CovalentDataTableModule,
  CovalentMessageModule,
  CovalentFileModule, } from '@covalent/core';

@NgModule({
  imports: [
    CovalentNotificationsModule,
    CovalentSearchModule,
    CovalentMenuModule,
    CovalentCommonModule,
    CovalentLayoutModule,
    CovalentMediaModule,
    CovalentExpansionPanelModule,
    CovalentStepsModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentMessageModule,
    CovalentFileModule,
  ],
  exports: [
    CovalentNotificationsModule,
    CovalentSearchModule,
    CovalentMenuModule,
    CovalentCommonModule,
    CovalentLayoutModule,
    CovalentMediaModule,
    CovalentExpansionPanelModule,
    CovalentStepsModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentMessageModule,
    CovalentFileModule,
  ]
})

export class CovalentModule {}
