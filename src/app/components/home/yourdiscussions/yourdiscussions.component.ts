import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-yourdiscussions',
  templateUrl: './yourdiscussions.component.html',
  styleUrls: ['./yourdiscussions.component.css']
})
export class YourdiscussionsComponent implements OnInit {
  date;
  month;
  year;
  constructor() { }

  ngOnInit() {
    this.date = new Date().getDate().toString();
    this.month = new Date().getMonth().toString();
    this.year = new Date().getFullYear().toString();
  }

}
